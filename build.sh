#!/usr/bin/env bash

rm -r dist gea_bot.pyz
pip install -r requirements.txt --target dist/

cp -r \
-t dist \
appliances appointments pin_codes users gea_bot telebot manage.py db.sqlite3

shiv --site-packages dist --compressed -p '/usr/bin/env python3' -o gea_bot.pyz -e gea_bot.main
