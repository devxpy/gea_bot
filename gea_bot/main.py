import multiprocessing
import os
import sys

import django

# setup django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gea_bot.settings")
django.setup()

from django.core.management import call_command
import gunicorn.app.wsgiapp as wsgi

# This is just a simple way to supply args to gunicorn
sys.argv = [".", "gea_bot.wsgi"] + sys.argv[1:]

gunicorn = multiprocessing.Process(target=wsgi.run)
gunicorn.start()

call_command("runtelebot")
gunicorn.join()
