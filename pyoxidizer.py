import hashlib
import os
import shutil
import subprocess
import tempfile
from distutils.spawn import find_executable
from distutils.sysconfig import get_python_lib
from functools import lru_cache
from pathlib import Path
from textwrap import dedent
from typing import Tuple

import requests

CACHE_DIR = Path.home() / ".cache/pyoxidizer"

PYTHON_BUILD_RELEASE_REPO = "indygreg/python-build-standalone"

GIT_REPO_URL = "https://github.com/indygreg/PyOxidizer.git"
GIT_REPO = CACHE_DIR / "PyOxidizer"

FINAL_OUTPUT = GIT_REPO / "target/debug/pyapp"


@lru_cache(None)
def _find_executable(name: str) -> str:
    path = find_executable(name)
    if path is None:
        raise ValueError(
            f"An executable for `{name}` could not be found. "
            f"Please ensure it is installed and available in PATH."
        )
    return path


class Executables:
    wget: str
    cargo: str
    git: str

    def __init__(self):
        self._executable_set = set(self.__class__.__annotations__)

    def __getattr__(self, name):
        if name not in self._executable_set:
            raise AttributeError(name)
        return _find_executable(name)


executables = Executables()


def _check_call(cmd: list, *args, **kwargs):
    print(f"$ {' '.join(map(str, cmd))}")
    return subprocess.check_call(cmd, *args, **kwargs)


@lru_cache(None)
def fetch_release(github_repo: str) -> Tuple[str, str]:
    api_url = f"https://api.github.com/repos/{github_repo}/releases/latest"
    data = requests.get(api_url).json()["assets"][0]
    return data["name"], data["browser_download_url"]


def download_python_build() -> Path:
    print("Fetching latest python build...")
    name, url = fetch_release(PYTHON_BUILD_RELEASE_REPO)
    path = CACHE_DIR / name
    if path.exists():
        with requests.get(url, stream=True) as r:
            try:
                if int(r.headers["Content-Length"]) == path.stat().st_size:
                    print("Done!")
                    return path
            except (TypeError, KeyError):
                pass
    try:
        path.parent.mkdir(parents=True)
    except FileExistsError:
        pass
    _check_call([executables.wget, "-c", "-O", path, url])
    return path


def ensure_git_repo() -> int:
    if not GIT_REPO.exists():
        return _check_call([executables.git, "clone", GIT_REPO_URL, GIT_REPO])
    return _check_call([executables.git, "pull"], cwd=GIT_REPO)


def pyoxidize(prog_name) -> int:
    python_build = download_python_build()
    ensure_git_repo()

    with open(python_build, "rb") as f:
        build_hash = hashlib.sha256(f.read()).hexdigest()

    config = dedent(
        f"""
        [python_distribution]
        local_path = "{python_build}"
        sha256 = "{build_hash}"
        
        [python_config]
        program_name = "{prog_name}"
        
        [python_packaging]
        module_paths = ["{get_python_lib()}"]
        """
    )
    print(config)
    with tempfile.NamedTemporaryFile("w") as f:
        f.write(config)
        f.seek(0)

        env = os.environ.copy()
        env["PYOXIDIZER_CONFIG"] = f.name

        return _check_call(
            [executables.cargo, "build", "--verbose"], env=env, cwd=GIT_REPO
        )


if __name__ == "__main__":
    pyoxidize("gea_bot")
    shutil.copy(FINAL_OUTPUT, Path.cwd())
